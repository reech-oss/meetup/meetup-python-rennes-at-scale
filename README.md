# Python At Scale - A Reech Case Study

<details>
<summary>Table of contents</summary>

- [Abstract](#abstract)
- [Project Setup](#project-setup)
- [Project Usage](#project-usage)
- [Tips](#tips)

</details>

## Abstract

Project used for the presentation at the [Meetup Python Rennes](https://www.meetup.com/python-rennes/events/284605135/).  
The slides supporting this presentation can be found in the document [Meetup Python Rennes - Python At Scale.pdf](assets/Meetup%20Python%20Rennes%20-%20Python%20At%20Scale.pdf).

## Project Setup

Virtual environment:

```bash
# The location of the Python interpreter may differ on you environment
virtualenv --python=${HOME}/.pyenv/versions/3.9.7/bin/python3 venv
source ./venv/bin/activate
# Install Pip in the virtual environment
python -m ensurepip --upgrade
python -m pip install --upgrade pip
```

Dependencies:

```bash
pip install celery==4.4.7
pip install faker
pip install pyyaml
```

Or:

```bash
pip install -r ./requirements.txt
```

Development requirements:

```bash
pip install -r ./requirements-dev.txt
```

## Project Usage

```bash
cd docker
docker-compose up -d

# To remove the containers and their volumes
# docker-compose down --volumes
```

The RabbitMQ Management console can be accessed at <http://localhost:15672/>.  
*The credentials can be found in the file [docker/.env](docker/.env).*

See the instructions in each `src/step_XX_*` folders:

- [step_01_monolith](src/step_01_monolith/README.md).
- [step_02_threading](src/step_02_threading/README.md).
- [step_03_queue](src/step_03_queue/README.md).
- [step_04_retry_logic](src/step_04_retry_logic/README.md).
- [step_05_auto_scale](src/step_05_auto_scale/README.md).

## Tips

To clean the `__pycache__` folders:

```bash
py3clean .
```
