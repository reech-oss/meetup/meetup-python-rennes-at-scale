# Types of social network.
BLOG = 'blog'
FACEBOOK = 'facebook'
INSTAGRAM = 'instagram'
PINTEREST = 'pinterest'
TIKTOK = 'tiktok'
TWITTER = 'twitter'
YOUTUBE = 'youtube'

# Message queue settings.
QUEUE_NAME = 'meetup_python_rennes'
TASK_CONSUMER_NAME = 'worker'
