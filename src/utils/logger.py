import logging
import logging.config
import yaml


def __create_logger_instance() -> logging.Logger:
    with open('logging.yml', 'r') as f:
        config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)

    return logging.getLogger('standard')


logger = __create_logger_instance()
