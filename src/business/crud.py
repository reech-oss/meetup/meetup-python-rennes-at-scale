from typing import Iterator
from faker import Faker
from src.business.profile import Profile


def get_candidate_profiles(social_type: str, count: int = 10) -> Iterator[Profile]:
    """
    Get the candidate profiles to update.

    Args:
        social_type (str): The type of the social network.
        count (int, optional): The maximum number of profiles to get. Defaults to 10.

    Yields:
        Iterator[Profile]: The profiles.
    """
    faker = Faker()

    for _ in range(count):
        yield Profile(social_type,
                      faker.random_number(digits=10),
                      faker.unique.first_name().lower())
