from time import sleep
from faker import Faker
from src.utils.logger import logger

_faker = Faker()
_API_CALL_DURATION = 1


class Profile:
    """
    Social network profile class.

    Disclaimer: all-in-one place for simplicity.
    """

    def __init__(self, social_type: str, social_id: str, slug: str) -> None:
        """
        Constructor.

        Args:
            social_type (str): The type of the social network.
            social_id (str): The identifier of the profile.
            slug (str): The slug of the profile.
        """
        self.social_type = social_type
        self.social_id = social_id
        self.slug = slug

    def __repr__(self) -> str:
        return f'{self.social_type}/{self.social_id} (@{self.slug})'

    def update(self, in_error: bool = False) -> None:
        """
        Update the profile with up-to-date information from the social network.

        Args:
            in_error (bool, optional): Whether the operation should fail or not. Defaults to False.
        """
        # Hack to simulate an operation failure (for demonstration purpose).
        if in_error:
            logger.fatal('Worker in error')
            raise RuntimeError()

        # Multi-stages process:
        content = self.__collect()  # 1-Collect
        self.__transform(content)   # 2-Transform
        self.__persist()            # 3-Store

    def __collect(self) -> dict:
        """
        Gather information from the social network.

        Returns:
            dict: The properties returned by the social network.
        """
        logger.info(f'Collect information from {self}')

        # Calling an external service may take time...
        sleep(_API_CALL_DURATION)

        return {
            'id': self.social_id,
            'slug': self.slug,
            'followers': _faker.random_number(),
            "following": _faker.random_number(),
            "posts": _faker.random_number(),
            'description': _faker.text(),
            'publications': [
                {
                    'id': _faker.random_number(),
                    'date': _faker.date_time_this_month(),
                    'text': _faker.text(),
                    'likes': _faker.random_number(),
                    'comments': _faker.random_number()
                } for _ in range(5)
            ]
        }

    def __transform(self, content: dict) -> None:
        """
        Prepare the profile with new content.

        Args:
            content (dict): The new properties of the profile.
        """
        pass

    def __persist(self) -> None:
        """
        Persist the content of the profile.
        """
        pass
