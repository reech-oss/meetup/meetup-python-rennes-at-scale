from src.business.crud import get_candidate_profiles
from src.step_04_retry_logic.worker import worker
from src.utils.logger import logger


def scheduler(social_type: str) -> None:
    for ticket, profile in enumerate(get_candidate_profiles(social_type), start=1):
        logger.info(f'Schedule profile {profile}')

        worker.delay(profile.social_type,
                     profile.social_id,
                     profile.slug,
                     in_error=ticket == 5)
