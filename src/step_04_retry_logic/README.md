# Step 4 - Retry Logic

<details>
<summary>Table of contents</summary>

- [Concepts](#concepts)
- [Usage](#usage)

</details>

## Concepts

- Give multiple chances to a message (until we decide to give up).

## Usage

```bash
# Schedule
python -m src.launcher 4

# Consume (worker)
celery worker \
    --hostname step_04_retry_logic \
    --app src.step_04_retry_logic.worker \
    --queues meetup_python_rennes \
    --concurrency=5
```

Check the logs for "Retrying task" warning messages.  
The process ID shows that a retried message can be processed by a different process.  
The message in error is retried `max_retries` times.
