from celery import Celery
from src.business.profile import Profile
from src.step_04_retry_logic import celeryconfig
from src.step_04_retry_logic.retry import RetryTask
from src.utils.constants import QUEUE_NAME, TASK_CONSUMER_NAME

app = Celery()
app.config_from_object(celeryconfig)


# Some arguments are added to the decorator: `bind`, `base` and `max_retries`
@app.task(name=TASK_CONSUMER_NAME, queue=QUEUE_NAME, bind=True, base=RetryTask, max_retries=3)
def worker(self, social_type: str, social_id: str, slug: str, in_error: bool = False) -> None:
    try:
        profile = Profile(social_type, social_id, slug)
        profile.update(in_error=in_error)
    except Exception as e:
        # Notify Celery to retry the message later (based on the `countdown` value)
        self.retry(countdown=2**self.request.retries, exc=e)
