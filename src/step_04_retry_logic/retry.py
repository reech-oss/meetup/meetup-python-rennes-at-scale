import json
from celery import Task
from src.utils.logger import logger


class RetryTask(Task):
    """
    Retry logic class.
    """

    def __format_args(self, task_id, args, kwargs):
        return json.dumps({'task_id': task_id, 'args': args} | kwargs)

    def on_retry(self, exc, task_id, args, kwargs, einfo):
        super(RetryTask, self).on_retry(exc, task_id, args, kwargs, einfo)

        logger.warn(f'Retrying task {self.__format_args(task_id, args, kwargs)}')

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        super(RetryTask, self).on_failure(exc, task_id, args, kwargs, einfo)

        logger.error(f'Failed to process task {self.__format_args(task_id, args, kwargs)}')
