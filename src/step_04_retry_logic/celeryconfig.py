from src.utils.constants import TASK_CONSUMER_NAME, QUEUE_NAME

# https://docs.celeryproject.org/en/latest/userguide/configuration.html#std-setting-task_serializer
task_serializer = 'json'
# https://docs.celeryproject.org/en/latest/userguide/configuration.html#std-setting-result_serializer
result_serializer = 'json'
# https://docs.celeryproject.org/en/latest/userguide/configuration.html#std-setting-accept_content
accept_content = ['json']

# https://docs.celeryproject.org/en/latest/userguide/configuration.html#std-setting-task_annotations
task_annotations = {
    TASK_CONSUMER_NAME: {}
}

# https://docs.celeryproject.org/en/latest/userguide/configuration.html#std-setting-task_routes
task_routes = {
    TASK_CONSUMER_NAME: {'queue': QUEUE_NAME}
}
