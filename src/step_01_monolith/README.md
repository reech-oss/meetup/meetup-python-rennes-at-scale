# Step 1 - Monolith

<details>
<summary>Table of contents</summary>

- [Concepts](#concepts)
- [Usage](#usage)

</details>

## Concepts

- Sequential processing.
- Does not scale.

## Usage

```bash
# Use time to observe sequential execution
time python -m src.launcher 1
```
