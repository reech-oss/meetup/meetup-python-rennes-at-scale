from src.business.crud import get_candidate_profiles
from src.utils.logger import logger


def scheduler(social_type: str) -> None:
    for profile in get_candidate_profiles(social_type):
        logger.info(f'Schedule profile {profile}')

        profile.update()
