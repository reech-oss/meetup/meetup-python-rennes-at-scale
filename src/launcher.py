import sys
import src.step_01_monolith.scheduler as step_01_monolith
import src.step_02_threading.scheduler as step_02_threading
import src.step_03_queue.scheduler as step_03_queue
import src.step_04_retry_logic.scheduler as step_04_retry_logic
import src.step_05_auto_scale.scheduler as step_05_autoscale
from src.utils.constants import INSTAGRAM


__SOCIAL_TYPE_DEFAULT = INSTAGRAM
__MODULES = {
    '1': lambda: step_01_monolith.scheduler(__SOCIAL_TYPE_DEFAULT),
    '2': lambda: step_02_threading.scheduler(__SOCIAL_TYPE_DEFAULT),
    '3': lambda: step_03_queue.scheduler(__SOCIAL_TYPE_DEFAULT),
    '4': lambda: step_04_retry_logic.scheduler(__SOCIAL_TYPE_DEFAULT),
    '5': lambda: step_05_autoscale.scheduler(__SOCIAL_TYPE_DEFAULT)
}

module_number = sys.argv[1] if len(sys.argv) > 1 else 0

module = __MODULES.get(module_number,
                       lambda: print('Incorrect module number [1-5]'))
module()
