from src.business.crud import get_candidate_profiles
from src.step_03_queue.worker import worker
from src.utils.logger import logger


def scheduler(social_type: str) -> None:
    for profile in get_candidate_profiles(social_type):
        logger.info(f'Schedule profile {profile}')

        # The call to the worker is queued by calling it with the `delay` method
        worker.delay(profile.social_type,
                     profile.social_id,
                     profile.slug)
