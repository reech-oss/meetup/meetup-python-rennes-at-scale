# Step 3 - Message Queue

<details>
<summary>Table of contents</summary>

- [Concepts](#concepts)
- [Usage](#usage)
  - [Concurrency](#concurrency)
  - [Rate Limit](#rate-limit)
- [Tips](#tips)
- [Resources](#resources)

</details>

## Concepts

- Loose coupling.
- Control on concurrency.
- Control on velocity.

## Usage

*Recall: the Docker container must be started.*

```bash
# Schedule
python -m src.launcher 3

# Navigate to the RabbitMQ Management console http://localhost:15672/ to see the messages created.

# Consume (worker)
celery worker \
    --hostname step_03_queue \
    --app src.step_03_queue.worker \
    --queues meetup_python_rennes \
    --pool=solo
```

Check the logs: one task is executed every second (the value of `_API_CALL_DURATION`).

Command Line Interface arguments:

```raw
celery worker \                       <= We run a worker
    --hostname step_03_queue \        <= The name of the host
    --app src.step_03_queue.worker \  <= The location of the application (module)
    --queues meetup_python_rennes \   <= The name of the message queue to handle
    --pool=solo                       <= Do not let Celery choose the size of the execution pool (more on that later)
```

### Concurrency

Easy scaling: use the argument `--concurrency=N` to set the size of the execution pool, i.e. the number of workers.

```bash
# Do not forget to exit the Celery workers

# Schedule
python -m src.launcher 3

# Consume (worker)
celery worker \
    --hostname step_03_queue \
    --app src.step_03_queue.worker \
    --queues meetup_python_rennes \
    --concurrency=5

# 5 workers + 1 leader
ps aux | grep "[c]elery"
```

### Rate Limit

Control the velocity at the command-line: `celery control`.  
*Can also be defined in the code.*

```bash
# Attention! The workers must be running.
TASK_CONSUMER_NAME=worker

# One message processed every 1 second
celery control rate_limit ${TASK_CONSUMER_NAME} '1/s'
python -m src.launcher 3

# One message processed every 5 seconds
celery control rate_limit ${TASK_CONSUMER_NAME} '12/m'
python -m src.launcher 3
```

Check the logs for "Collect information" messages.  
The time between two executions should be around the `rate_limit` value.

## Tips

The setting [`task_always_eager`](https://docs.celeryproject.org/en/stable/userguide/configuration.html#task-always-eager) (old-style `CELERY_ALWAYS_EAGER`) can be set to `True` to bypass the scheduling mechanism.  
The worker code will be called directly, which can be handy when testing (no message broker is required).

## Resources

Few resources to help you start:

- [How to Use Celery for Scheduling Tasks](https://www.caktusgroup.com/blog/2021/08/11/using-celery-scheduling-tasks/).
- [Celery Execution Pools: What is it all about?](https://www.distributedpython.com/2018/10/26/celery-execution-pool/)
