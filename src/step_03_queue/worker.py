from celery import Celery
from src.business.profile import Profile
from src.step_03_queue import celeryconfig
from src.utils.constants import QUEUE_NAME, TASK_CONSUMER_NAME

# A Celery application is created to support the messaging
app = Celery()
app.config_from_object(celeryconfig)


# A decorator is added to the function that supports the task
@app.task(name=TASK_CONSUMER_NAME, queue=QUEUE_NAME)
def worker(social_type: str, social_id: str, slug: str, in_error: bool = False) -> None:
    profile = Profile(social_type, social_id, slug)
    profile.update(in_error=in_error)
