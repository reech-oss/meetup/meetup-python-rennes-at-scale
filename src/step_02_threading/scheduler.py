from concurrent.futures import ThreadPoolExecutor
from src.business.crud import get_candidate_profiles
from src.step_02_threading.worker import worker
from src.utils.logger import logger


def scheduler(social_type: str) -> None:
    with ThreadPoolExecutor() as pool:
        for ticket, profile in enumerate(get_candidate_profiles(social_type), start=1):
            logger.info(f'Schedule profile {profile}')

            pool.submit(worker, profile, in_error=ticket == 5)
