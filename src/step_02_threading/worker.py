from src.business.profile import Profile


def worker(profile: Profile, in_error: bool = False) -> None:
    profile.update(in_error=in_error)
