# Step 2 - Multithreading

<details>
<summary>Table of contents</summary>

- [Concepts](#concepts)
- [Usage](#usage)
- [Implementation Notes](#implementation-notes)

</details>

## Concepts

- We must keep in mind multi-threading while writing code.
- In case of exception:
  - Some state must be saved.
  - Some retry logic must be implemented.
- Will only scale vertically, but not horizontally.

## Usage

```bash
# Use time to observe "parallel" execution
time python -m src.launcher 2
```

## Implementation Notes

The scheduler could be more traditionally written using the [`threading`](https://docs.python.org/3.9/library/threading.html) module instead of the [`ThreadPoolExecutor`](https://docs.python.org/3.9/library/concurrent.futures.html):

```python
import threading

def scheduler(social_type: str):
    for ticket, profile in enumerate(get_candidate_profiles(social_type), start=1):
        logger.info(f'Schedule profile {profile}')

        threading.Thread(target=worker,
                         args=[profile],
                         kwargs={'in_error': ticket == 5},
                         name=f'Thread-{ticket}').start()
```
