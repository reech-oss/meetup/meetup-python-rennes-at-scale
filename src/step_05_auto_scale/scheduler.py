import math
from time import sleep
from src.business.crud import get_candidate_profiles
from src.step_05_auto_scale.worker import worker
from src.utils.logger import logger


def scheduler(social_type: str) -> None:
    for step in range(6):
        # 1, 2, 7, 20, 54, 148
        count = int(math.exp(step))
        logger.info(f'Schedule {count} messages')

        for profile in get_candidate_profiles(social_type, count=count):
            worker.delay(profile.social_type, profile.social_id, profile.slug)

        sleep(5)
