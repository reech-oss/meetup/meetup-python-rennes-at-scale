# Step 5 - Auto Scaling

<details>
<summary>Table of contents</summary>

- [Concepts](#concepts)
- [Usage](#usage)

</details>

## Concepts

- The number of workers is automatically adjusted with respect to the number of waiting messages.

## Usage

Use auto-scaling (argument `--autoscale=MAX,MIN`):

1. Run Celery application

    ```bash
    celery worker \
        --hostname step_05_auto_scale \
        --app src.step_05_auto_scale.worker \
        --queues meetup_python_rennes \
        --autoscale=30,1
    ```

2. Follow the number of allocated workers (up and down):

    ```bash
    while :; do expr $(ps aux | grep "[c]elery" | wc -l) - 1; sleep 1; done
    ```

3. Schedule jobs:

    ```bash
    python -m src.launcher 5
    ```

When all the messages are consumed, wait few tens of seconds to observe the decrease of the number of workers.
