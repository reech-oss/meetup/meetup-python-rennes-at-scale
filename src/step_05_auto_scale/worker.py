from celery import Celery
from src.business.profile import Profile
from src.step_05_auto_scale import celeryconfig
from src.utils.constants import QUEUE_NAME, TASK_CONSUMER_NAME

app = Celery()
app.config_from_object(celeryconfig)


@app.task(name=TASK_CONSUMER_NAME, queue=QUEUE_NAME)
def worker(social_type: str, social_id: str, slug: str, in_error: bool = False) -> None:
    profile = Profile(social_type, social_id, slug)
    profile.update(in_error=in_error)
